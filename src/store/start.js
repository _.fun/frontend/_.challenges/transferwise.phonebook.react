import { clearSearchAction } from 'components/phonebook/actions'

export default ({ dispatch }) => {
  clearSearchAction(dispatch)()
}
